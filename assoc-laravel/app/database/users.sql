drop table if exists users;
drop table if exists comment;

create table users (
    id integer not null primary key autoincrement,
    title varchar(80) ,
    message varchar(120),
    name varchar(80)
); 
create table comment (
    id integer not null primary key autoincrement,
    message varchar(120),
    name varchar(80) not null,
    user_id integer not null
); 
insert into users values (null, "GUS",  "GUS",  "GUS");