<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
  $posts = get_posts();
	return View::make('social.home')->withPosts($posts);
});

Route::post('add_post', function()
{
  $name = Input::get('name');
  $message = Input::get('message');
  $title = Input::get('title');
  
  $id = add_post($name, $title, $message);

  // If successfully created then display newly created item
  if ($id) 
  {
    return Redirect::to(url("./"));
  } 
  else
  {
    die("Error adding item");
  }
});

//change this to to the edit page
//create the edit page
Route::get('update_post/{id}', function ($id)
{
  $post = get_post($id);
	return View::make('social.edit')->withPost($post);
}); 

Route::post('edit_post_action', function()
{
  $title = Input::get('title');
  $message = Input::get('message');
  $name = Input::get('name');
  $id = Input::get('id');
  //update post
 
  update_post($id, $name, $title, $message);
  
    return Redirect::to(url("./"));
 
});

Route::get('delete_post_action/{id}', function($id)
{
  delete_post($id);
	 return Redirect::to(url("./"));
});

//functions

function get_posts()
{
  $sql = "select * from users ORDER BY id DESC";
  $posts = DB::select($sql);
  return $posts;
}

/* Gets item with the given id */
function get_post($id)
{
	$sql = "select id, title, name, message from users where id = ?";
	$items = DB::select($sql, array($id));

	// If we get more than one item or no items display an error
	if (count($items) != 1) 
	{
    die("Invalid query or result: $query\n");
  }

	// Extract the first item (which should be the only item)
  $item = $items[0];
	return $item;
}

function add_post($name, $title, $message) 
{
  $sql = "insert into users (name, title, message) values (?, ?, ?)";

  DB::insert($sql, array($name, $title, $message));

  $id = DB::getPdo()->lastInsertId();

  return $id;
}

function delete_post($id) 
{
  $sql = "delete from users where id = ?";
  DB::delete($sql, array($id));
}

function update_post($id, $name, $title, $message)
{
  $sql = "update users set name = ?, title = ?, message = ? where id = ?";
  DB::update($sql, array($name, $title, $message, $id));
}